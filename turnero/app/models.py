from django.db import models

# Create your models here.


class Parametro(models.Model):
    parametro_id = models.AutoField(primary_key=True)
    parametro_descripcion = models.CharField(max_length=25,blank=False,null=False)
    parametro_tipo_dato = models.CharField(max_length=25,blank=False,null=False)
    parametro_valor = models.CharField(max_length=50,blank=False,null=False)

    def __str__(self) -> str:
        return self.parametro_descripcion

class Permiso(models.Model):
    permiso_id = models.AutoField(primary_key=True)
    permiso_descripcion = models.CharField(max_length=25,blank=False,null=False)

    def __str__(self) -> str:
        return self.permiso_descripcion 

class Rol(models.Model):
    rol_id = models.AutoField(primary_key=True)
    permiso_id = models.ForeignKey('Permiso', on_delete=models.CASCADE)
    rol_descripcion = models.CharField(max_length=25,blank=False,null=False)

    def __str__(self) -> str:
        return self.rol_descripcion 

class Persona(models.Model):
    persona_id = models.AutoField(primary_key=True)
    persona_cedula = models.CharField(max_length=15,blank=False,null=False)
    persona_nombre = models.CharField(max_length=50,blank=False,null=False)
    persona_apellido = models.CharField(max_length=50,blank=False,null=False)
    persona_fecha_nacimineto = models.DateField(blank=False,null=False)
    persona_direccion = models.CharField(max_length=50,blank=False,null=False)
    persona_telefono = models.CharField(max_length=15,blank=False,null=False)
    persona_correo = models.CharField(max_length=25,blank=False,null=False)

    def __str__(self) -> str:
        return self.persona_cedula

class TipoPersona(models.Model):
    tipo_persona_id = models.AutoField(primary_key=True)
    tipo_persona_descripcion = models.CharField(max_length=25,blank=False,null=False)

    def __str__(self) -> str:
        return self.tipo_persona_descripcion 

class Servicio(models.Model):
    servicio_id = models.AutoField(primary_key=True)
    servicio_descripcion = models.CharField(max_length=50,blank=False,null=False)
    servicio_estado = models.BooleanField(default=True,blank=False,null=False)
    servicio_cantidad_cola = models.SmallIntegerField(blank=False,null=False)

    def __str__(self) -> str:
        return self.servicio_descripcion 

class Usuario(models.Model):
    usuario_id = models.AutoField(primary_key=True)
    usuario_nombre = models.CharField(max_length=25,blank=False,null=False)
    persona_id = models.ForeignKey('Persona', on_delete=models.CASCADE)
    rol_id = models.ForeignKey('Rol', on_delete=models.CASCADE)
    usuario_fecha_alta = models.DateField(blank=False,null=False)
    usuario_fecha_baja = models.DateField(blank=True, null=True)
    usuario_estado = models.BooleanField(default=True,blank=False,null=False)
    def __str__(self) -> str:
        return self.usuario_nombre 

class NivelPrioridad(models.Model):
    nivel_prioridad_id = models.AutoField(primary_key=True)
    nivel_prioridad_condicion = models.CharField(max_length=50,blank=False,null=False)
    nivel_prioridad_prioridad = models.SmallIntegerField(blank=False,null=False)
    nivel_prioridad_estado = models.BooleanField(default=True,blank=False,null=False)

    def __str__(self) -> str:
        return self.nivel_prioridad_condicion 


class Cola(models.Model):
    cola_id = models.AutoField(primary_key=True)
    servicio_id = models.ForeignKey('Servicio', on_delete=models.SET_DEFAULT, default=None,blank=True,  null=True)
    persona_id = models.ForeignKey('Persona', on_delete=models.SET_DEFAULT, default=None,blank=True,  null=True)
    nivel_prioridad_id = models.ForeignKey('NivelPrioridad', on_delete=models.SET_DEFAULT, default=None,blank=True,  null=True)
    usuario_id = models.CharField(max_length=50,blank=True,null=True)
    cola_ticket_nro = models.SmallIntegerField(blank=False,null=False)
    cola_fecha_hora_ingreso = models.DateTimeField(blank=True,null=True)
    cola_fecha_hora_salida = models.DateTimeField(blank=True,null=True)
    cola_fecha_hora_atencion = models.DateTimeField(blank=True, null=True)
    cola_estado = models.CharField(max_length=50,blank=True,null=True)



class Tickets(models.Model):
    tickets_id = models.AutoField(primary_key=True)
    tickets_descripcion = models.CharField(max_length=20,blank=True,null=True)
    tickets_nro = models.BigIntegerField(blank=False,null=False)

    def __str__(self) -> str:
        return self.tickets_descripcion 
