INDICACIONES

Puesta a punto
Instalar Python y Pip
[Python] (https://www.python.org/downloads/)

[pip] (https://pip.pypa.io/en/stable/)
Luego instalar el virtualenv

pip install virtualenv


Luego crear e iniciar el entorno virtual


python -m venv turnero-env



.\turnero-env\Scripts\activate


Luego instalar las dependencias

pip install -r requirements.txt



Ejecucion del proyecto
Para ello debemos siempre estar dentro de nuestro entorno virtual y ejecutamos la siguiente línea.

python .\manage.py runserver



Para detener la ejecucio
Ctrl + c

Para salir del entorno virtual

deactivate
